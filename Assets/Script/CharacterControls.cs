﻿using UnityEngine;
using System.Collections;

public class CharacterControls : MonoBehaviour{
	public Animator animator;
	public NavMeshAgent WalkArea = null;
	public GameObject enemy = null;
	public GameObject Marker = null;
	//public GameObject Cursor;


	void Awake () 
	{
		animator = GetComponent<Animator>();
		animator.Play ("KK_Idle");
	}

	void Update()
	{	
		if (Input.GetMouseButtonDown(0))
		{
			animator = GetComponent<Animator>();
			animator.Play ("KK_Run");
			Debug.Log ("Test");
			RaycastHit Hit;
			Ray Ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast (Ray, out Hit, 100)) {
				if (Hit.transform.tag == "WalkableGround") {
					Instantiate (Marker, Hit.point, Quaternion.identity);
					WalkArea.Resume ();
					WalkArea.SetDestination (Hit.point);
				}


				if (Hit.transform.tag == "Enemy") {
					GetComponent<NavMeshAgent> ().destination = enemy.transform.position;
				}
			}
		}
	}
	void OnTriggerStay(Collider collide){
		if (collide.gameObject.tag == "pointer") {
			animator.Play ("KK_Attack_Standy");
		}
		if (collide.gameObject.tag == "Enemy") {
			animator.Play ("KK_Attack");
		}
	}
	void OnTriggerExit(Collider collide){
		animator.Play ("KK_Attack_Standy");
		}
}