﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
public class Spawner : MonoBehaviour {
	
	public float spawnTime = 0.0f;
	public float spawnDelay = 0.0f;
	public GameObject food;
	public float FoodCount;
	private float totalFoodCount = 10;
    public Color renderercolor;
	
	void Start (){

		InvokeRepeating("SpawnFood", spawnDelay, spawnTime);
   

    }
	
	void SpawnFood ()
	{
		if (totalFoodCount <= FoodCount)
			return;
		Vector3 position = new Vector3 (Random.Range (-25.0f, 25.0f), -1, Random.Range (-25.0f, 25.0f));
		Instantiate(food, position, Quaternion.identity);
       
    }
}