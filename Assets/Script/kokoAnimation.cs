﻿using UnityEngine;
using System.Collections;

public class kokoAnimation : MonoBehaviour {
	public Animator animator;
	//	public bool Play = ("KK_Run", PlayMode mode = PlayMode.StopSameLayer);
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void PlayRun() {
		animator = GetComponent<Animator>();
		animator.Play ("KK_Run");
		animator.Play ("WP_Run");
		animator.Play ("GB_Run");
		animator.Play ("SL_Run");
	}
	public void PlayDamage() {
		animator = GetComponent<Animator>();
		animator.Play ("KK_Damage");
		animator.Play ("WP_Damage");
		animator.Play ("GB_Damage");
		animator.Play ("SL_Damage");
		//animator.SetInteger ("animationNumber", 2);
	}
	public void PlayAttack() {
		animator = GetComponent<Animator>();
		animator.Play ("KK_Attack");
		animator.Play ("WP_Attack");
		animator.Play ("GB_Attack");
		animator.Play ("SL_Attack");

	}
	public void PlayAttackStanby() {
		animator = GetComponent<Animator>();
		animator.Play("KK_Attack_Standy");
		animator.Play ("WP_Attack01");
		animator.Play ("GB_Attack01");
		animator.Play ("SL_Attack01");
	}
	public void PlayDead() {
		animator = GetComponent<Animator>();
		animator.Play("KK_Dead");
		animator.Play ("WP_Dead");
		animator.Play ("GB_Dead");
		animator.Play ("SL_Dead");
	}
	public void PlayDrawBlade() {
		animator = GetComponent<Animator>();
		animator.Play("KK_DrawBlade");
	}
	public void PlayPutBlade() {
		animator = GetComponent<Animator>();
		animator.Play("KK_PutBlade");
	}
	public void PlayCobo() {
		animator = GetComponent<Animator>();
		animator.Play("KK_Combo");
	}
	public void PlaySkill() {
		animator = GetComponent<Animator>();
		animator.Play("KK_Skill");
	}
	public void PlayAttackRun() {
		animator = GetComponent<Animator>();
		animator.Play("KK_Run_No");
		animator.Play ("WP_Walk");
		animator.Play ("GB_Walk");
		animator.Play ("SL_Walk");
	}
	public void PlayIdle() {
		animator = GetComponent<Animator>();
		animator.Play("KK_Idle");
		animator.Play ("WP_Idle");
		animator.Play ("GB_Idle");
		animator.Play ("SL_Idle");
		animator.Play ("Idle");
	}
	public void PlayTalk() {
		animator = GetComponent<Animator>();
		animator.Play ("talk");
	}

//	public void PlayRun() {
//		animator = GetComponent<Animator>();
//		animator.SetBool ("KK_Run", true);
//	}
//	public void PlayRun() {
//		animator = GetComponent<Animator>();
//		animator.SetBool ("KK_Run", true);
//	}
//	public void PlayRun() {
//		animator = GetComponent<Animator>();
//		animator.SetBool ("KK_Run", true);
//	}
//	public void PlayRun() {
//		animator = GetComponent<Animator>();
//		animator.SetBool ("KK_Run", true);
//	}
//	public void PlayRun() {
//		animator = GetComponent<Animator>();
//		animator.SetBool ("KK_Run", true);
//	}
//	public void PlayRun() {
//		animator = GetComponent<Animator>();
//		animator.SetBool ("KK_Run", true);
//	}
//	public void PlayRun() {
//		animator = GetComponent<Animator>();
//		animator.SetBool ("KK_Run", true);
//	}
//	public void PlayRun() {
//		animator = GetComponent<Animator>();
//		animator.SetBool ("KK_Run", true);
//	}
//	public void PlayRun() {
//		animator = GetComponent<Animator>();
//		animator.SetBool ("KK_Run", true);
//	}
}
