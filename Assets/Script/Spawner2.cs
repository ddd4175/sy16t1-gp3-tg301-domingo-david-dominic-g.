﻿using UnityEngine;
using System.Collections;

public class Spawner2 : MonoBehaviour {
	
	public float spawnTime = 0.0f;
	public float spawnDelay = 0.0f;
	public GameObject[] enemy;
	private float totalenemyCount = 10;
	
	void Start (){

		InvokeRepeating("Spawn", spawnDelay, spawnTime);
   

    }
	
	void Spawn ()
	{
		Vector3 position = new Vector3 (Random.Range (-50.0f, 50.0f), 1f, Random.Range (-50.0f, 50.0f));
		Instantiate(enemy[Random.Range(0, enemy.Length)], position, Quaternion.identity);
       
    }
}