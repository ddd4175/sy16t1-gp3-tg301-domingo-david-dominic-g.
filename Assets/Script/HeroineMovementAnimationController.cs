﻿using UnityEngine;
using System.Collections;

public class HeroineMovementAnimationController : MonoBehaviour
{
	public AnimationClip Idle;
	public AnimationClip Run;
	public AnimationClip Right;
	public AnimationClip Left;
	public AnimationClip Back;
	public AnimationClip Jump;
	public AnimationClip Attack;

	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetAxis ("Horizontal") > 0.1f)
						gameObject.GetComponent<Animation>().CrossFade (Right.name);
				else if (Input.GetAxis ("Horizontal") < -0.1f)
						gameObject.GetComponent<Animation>().CrossFade (Left.name);
				else if (Input.GetAxis ("Vertical") > 0.1f)
						gameObject.GetComponent<Animation>().CrossFade (Run.name);
				else if (Input.GetAxis ("Vertical") < -0.1f)
						gameObject.GetComponent<Animation>().CrossFade (Back.name);
				else if (Input.GetButton ("Jump"))
						gameObject.GetComponent<Animation>().CrossFade (Jump.name);
		else
			gameObject.GetComponent<Animation>().CrossFade(Idle.name);
		if (Input.GetButton ("Fire1")) {
						GetComponent<Animation>().Play ("attack1", PlayMode.StopAll);
			//animation.Play ("attack2", PlayMode.StopAll);
			//animation.Play ("attack3", PlayMode.StopAll);
			//animation.Play ("attack4", PlayMode.StopAll);

				}
	}
}
