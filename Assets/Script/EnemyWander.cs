﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyWander : MonoBehaviour {
	public Animator animator;
	public float wanderRadius;
	public float wanderTimer;
	private GameObject target;
	private NavMeshAgent agent;
	public Rigidbody enemyMovement;

	private float timer;

	// Use this for initialization
	void Start () {
		
	}

	void OnEnable () {
		agent = GetComponent<NavMeshAgent> ();
		timer = wanderTimer;
	}

	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;

		if (timer >= wanderTimer) {
			Vector3 newPos = WanderArea(transform.position, wanderRadius, -1);
			agent.SetDestination(newPos);
//			animator = GetComponent<Animator>();
//			animator.Play ("WP_Walk");
//			animator.Play ("GB_Walk");
//			animator.Play ("SL_Walk");
			timer = 0;
		}
//		enemyMovement = GetComponent<Rigidbody> ();
//		if (enemyMovement.velocity == Vector3.zero) {
////			animator = GetComponent<Animator>();
////			animator.Play ("WP_Idle");
////			animator.Play ("GB_Idle");
////			animator.Play ("SL_Idle");
//		}
	}

	public Vector3 WanderArea(Vector3 origin, float dist, int layermask) {
		
		Vector3 randDirection = Random.insideUnitSphere * dist;
		randDirection += origin;
		NavMeshHit navHit;
		NavMesh.SamplePosition (randDirection, out navHit, dist, layermask);
		return navHit.position;
	}
//	void OnTriggerEnter(){
//		transform.position = Vector3.MoveTowards(transform.position, target.transform.position, 3);
//	}
}
